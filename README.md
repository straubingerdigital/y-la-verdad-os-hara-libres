*y conoceréis la verdad, y la verdad os hará libres* ~San Juan 8, 32.

## “Y conoceréis la verdad, y la verdad os hará libres”: Contestación Católica al librito *“El conocimiento que lleva a vida eterna”* de los Testigos de Jehová

Esta publicación no fue escrita ni inspirada por Dr. Monseñor Juan Straubinger, sin embargo aparece dentro del proyecto [Straubinger Digital](https://bitbucket.org/straubingerdigital/) por razones de conveniencia.

## Licencia

![Licencia Creative Commons BY-SA 4.0 Internacional](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

*“Y conoceréis la verdad, y la verdad os hará libres”: Contestación Católica al librito “El conocimiento que lleva a vida eterna” de los Testigos de Jehová* por [Jorge Javier Araya Navarro](https://bitbucket.org/straubingerdigital/) se distribuye bajo una [Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es).
